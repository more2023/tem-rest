# tem-rest

#### 介绍
基于springboot2.x轻量级框架的rest接口系统，基于token认证方式，支持多数据源，支持注解方式参数加解密，支持分布式任务调度，统一封装请求响应编码风格，简化开发。

#### 软件架构
软件架构说明


#### 部署教程

1.  服务部署目录文件结构。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/164218_3f7f2ea3_676656.png "屏幕截图.png")

2.  依次进入各服务器，服务部署目录下，使用 ./start.sh start 启动服务；  ./start.sh stop 停止服务（可使用shell脚本群起服务）。
3.  服务日志目录： logs/record.log
4.  nginx进行https代理，及多服务部署负载均衡。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/165722_961ba0e3_676656.png "屏幕截图.png")

#### 交流反馈
1. 有问题先看看 F&Q 中有没有相关的回答
2. 欢迎提交ISSUS，请写清楚问题的具体原因，重现步骤和环境(上下文)
3. 项目/微服务交流请进群：1085211067
4. 个人博客：https://www.jianshu.com/u/c6a2bcc46f1d
