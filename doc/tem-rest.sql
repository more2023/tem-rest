-- 1、字典类型表
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典类型表';

-- 2、字典数据表
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典数据主键',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `dict_code` varchar(100) DEFAULT '' COMMENT '字典编码',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典值',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `css_class` varchar(500) DEFAULT '' COMMENT '样式属性',
  `is_default` varchar(2) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`data_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';

-- 3、认证配置表
DROP TABLE IF EXISTS `sys_uri_auth`;
CREATE TABLE `sys_uri_auth` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT 'uri认证ID',
  `uri` varchar(50) NOT NULL COMMENT '请求uri',
  `is_auth` varchar(2) DEFAULT '0' COMMENT '是否认证(0:是；1:否)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='URI需token认证配置表';