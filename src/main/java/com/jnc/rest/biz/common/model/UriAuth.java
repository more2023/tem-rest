package com.jnc.rest.biz.common.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Auther: jjn
 * @Date: 2020/4/23 15:26
 * @Desc:
 */
@Setter
@Getter
public class UriAuth {

    private Integer id;
    private String uri;
    private String isAuth;

    @Override
    public String toString() {
        return "UriAuth{" +
                "id=" + id +
                ", uri='" + uri + '\'' +
                ", isAuth='" + isAuth + '\'' +
                '}';
    }
}
