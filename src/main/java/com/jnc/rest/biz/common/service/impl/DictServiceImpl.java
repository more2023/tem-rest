package com.jnc.rest.biz.common.service.impl;

import com.google.common.collect.Maps;
import com.jnc.rest.biz.common.mapper.DictMapper;
import com.jnc.rest.biz.common.model.DictData;
import com.jnc.rest.biz.common.model.UriAuth;
import com.jnc.rest.biz.common.service.DictService;
import com.jnc.rest.constant.RedisConstant;
import com.jnc.rest.core.config.redis.RedisStore;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:57
 * @Desc:
 */
@Slf4j
@Service
public class DictServiceImpl implements DictService {

    @Autowired
    DictMapper dictMapper;
    @Autowired
    RedisStore redisStore;

    /**
     * 根据字典类型，获取字典数据列表
     * @param dictType
     * @return
     */
    @Override
    public List<DictData> getData(String dictType) {
        return dictMapper.getDictDataByType(dictType);
    }

    /**
     * 根据字典类型、字典编码，获取字典值
     * @param dictType
     * @param dictCode
     * @return
     */
    @Override
    public String getValue(String dictType, String dictCode) {
        DictData model = new DictData();
        model.setDictType(dictType);
        model.setDictCode(dictCode);
        List<DictData> list = dictMapper.getDictDataValue(model);
        if(list != null && list.size() > 0){
            return list.get(0).getDictValue();
        }
        return null;
    }

    @Override
    public void loadRedisDictData() {
        //获取所有有效的字典值
        List<DictData> dictDataList = dictMapper.queryDictData();
        List<String> dictTypeList = dictMapper.queryDictType();

        if(dictTypeList == null || dictTypeList.size() < 1){
            log.warn("No valid dict type ...");
            return;
        }
        if(dictDataList == null || dictDataList.size() < 1){
            log.warn("No valid dict data ...");
            return;
        }

        for (String type : dictTypeList){
            Map<String, Object> map = Maps.newHashMap();
            for (DictData obj : dictDataList){
                if(type.equals(obj.getDictType())){
                    map.put(obj.getDictCode(), obj.getDictValue());
                }
            }
            //先清空老的缓存数据
            redisStore.del(RedisConstant.DICT_DATA + type);
            //再存入新的缓存数据
            redisStore.putHashValues(RedisConstant.DICT_DATA + type, map);
        }
    }

    /**
     * 根据字典类型、字典编码，获取字典值(先从缓存取，缓存没有再从数据库取)
     * @param dictType
     * @param dictCode
     * @return
     */
    @Override
    public String getValueBy(String dictType, String dictCode) {
        String key = RedisConstant.DICT_DATA + dictType;
        String hashValues = (String) redisStore.getHashValues(key, dictCode);
        //为空，则从数据库里取，并设置到redis
        if(StringUtils.isBlank(hashValues)){
            boolean flag = redisStore.exists(key);
            if(flag){
                hashValues = getValue(dictType, dictCode);
                redisStore.putHashValue(key, dictCode, hashValues);
            }else{
                List<DictData> list = dictMapper.getDictDataByType(dictType);
                Map<String, Object> map = Maps.newHashMap();
                for (DictData obj : list){
                    map.put(obj.getDictCode(), obj.getDictValue());
                    if(dictCode.equals(obj.getDictCode())){
                        hashValues = obj.getDictValue();
                    }
                }
                //先清空老的缓存数据
                redisStore.del(key);
                //再存入新的缓存数据
                redisStore.putHashValues(key, map);
            }
        }
        return hashValues;
    }

    @Override
    public List<UriAuth> queryUri() {
        return dictMapper.queryUri();
    }
}
