package com.jnc.rest.biz.common.service;

import com.jnc.rest.biz.common.model.DictData;
import com.jnc.rest.biz.common.model.UriAuth;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:54
 * @Desc:
 */
public interface DictService {

    public List<DictData> getData(String dictType);
    public String getValue(String dictType, String dictCode);

    public void loadRedisDictData();

    public String getValueBy(String dictType, String dictCode);

    public List<UriAuth> queryUri();
}
