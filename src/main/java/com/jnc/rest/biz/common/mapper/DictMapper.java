package com.jnc.rest.biz.common.mapper;

import com.jnc.rest.biz.common.model.DictData;
import com.jnc.rest.biz.common.model.UriAuth;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/4/15 18:56
 * @Desc:
 */
@Mapper
public interface DictMapper {

    public List<DictData> queryDictData();
    public List<DictData> getDictDataByType(String dictType);
    public List<DictData> getDictDataValue(DictData record);
    public List<String> queryDictType();

    /**
     * 获取需要token认证的 URI
     * @return
     */
    public List<UriAuth> queryUri();
}
