package com.jnc.rest.biz.common.controller;

import com.jnc.rest.biz.common.model.DictData;
import com.jnc.rest.biz.common.service.DictService;
import com.jnc.rest.core.base.controller.BaseController;
import com.jnc.rest.core.base.dto.BaseResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2020/4/16 14:03
 * @Desc: 字典码值controller
 */
@Slf4j
@RestController
@RequestMapping("/biz/dict")
public class DictController extends BaseController {

    @Autowired
    DictService dictService;

    /**
     * 刷新字典表缓存数据
     * @return
     */
    @GetMapping("/loadRedisDictData")
    public BaseResp loadRedisDictData(){
        dictService.loadRedisDictData();
        return BaseResp.success("加载成功");
    }

    /**
     * 根据字典类型，获取字典值列表
     * @param dictType
     * @return
     */
    @GetMapping("/getData/{dictType}")
    public BaseResp<List<DictData>> getData(@PathVariable("dictType") String dictType){
        log.info("DictController.getData.req: {}", dictType);
        List<DictData> list = dictService.getData(dictType);
        return BaseResp.success(list);
    }
}
