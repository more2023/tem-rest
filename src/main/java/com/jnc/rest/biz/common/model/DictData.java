package com.jnc.rest.biz.common.model;

import com.jnc.rest.core.base.dto.BaseReq;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class DictData extends BaseReq {
    private static final long serialVersionUID = 1L;

    private Integer dataId;

    private String dictType;

    private String dictCode;

    private String dictValue;

    private Integer dictSort;

    private String cssClass;

    private String isDefault;

    private Integer status;

    private Date createTime;

    private String remark;

    private String dictName;


    @Override
    public String toString() {
        return "DictData{" +
                "dataId=" + dataId +
                ", dictType='" + dictType + '\'' +
                ", dictCode='" + dictCode + '\'' +
                ", dictValue='" + dictValue + '\'' +
                ", dictSort=" + dictSort +
                ", cssClass='" + cssClass + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}