package com.jnc.rest.biz.job.handler;

import com.jnc.rest.biz.job.task.DemoTask;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * @Auther: jjn
 * @Date: 2020/5/18 16:33
 * @Desc: 定时任务handler
 */
@Slf4j
@Component
public class DemoJobHandler {

    @Autowired
    private ThreadPoolTaskExecutor customThreadPoolTaskExecutor;

    /**
     * demo任务
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("demo1JobHandler")
    public ReturnT<String> demo1JobHandler(String param) throws Exception {
        long startTime = System.currentTimeMillis();
        XxlJobLogger.log("开始demo1任务");
        log.debug("开始demo1任务");

        //异步执行任务
        DemoTask task = new DemoTask();
        task.setUrl("http://xxxx.com");
        task.setJson(param);
        customThreadPoolTaskExecutor.execute(task);

        long endTime = System.currentTimeMillis();
        XxlJobLogger.log("结束demo1任务，花费时间：" + (endTime - startTime));
        log.debug("结束demo1任务, 花费时间：{}", (endTime - startTime));
        return ReturnT.SUCCESS;
    }
}
