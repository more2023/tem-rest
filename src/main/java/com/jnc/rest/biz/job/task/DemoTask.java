package com.jnc.rest.biz.job.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jnc.rest.util.OkHttpUtil;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * @Auther: jjn
 * @Date: 2020/4/13 17:57
 * @Desc: Demo异步任务
 */
@Slf4j
@Setter
public class DemoTask implements Runnable {

    private String url;
    private String json;

    /**
     * 异步执行任务
     */
    @Override
    public void run() {
        OkHttpUtil okHttpUtil = OkHttpUtil.getInstance();
        try {
            Response response = okHttpUtil.postJson(url, json);
            log.info(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
