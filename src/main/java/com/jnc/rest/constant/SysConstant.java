package com.jnc.rest.constant;

/**
 * @Auther: jjn
 * @Date: 2019/12/6 17:10
 * @Desc: 系统常量类
 */
public class SysConstant {

    public static final String CHARSET_NAME = "UTF-8";   //字符集编码

    public static final int HTTP_CODE_SUCCESS = 200;     //http成功状态码

    public static final String TX_BASE_NAME = "baseTransactionManager";  //基础事务管理器名称



    public static String PAGE_NUM = "pageNum";   //当前页码

    public static String PAGE_SIZE = "pageSize";  //每页显示记录数

    public static String ORDER_BY_COLUMN = "orderByColumn";  //排序列

    public static String ORDER_RULE = "orderRule";   //排序规则(desc 或 asc)


    public static final String TOKEN_KEY = "Authorization";   //header头部token的key
}
