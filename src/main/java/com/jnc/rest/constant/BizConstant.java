package com.jnc.rest.constant;

/**
 * @Auther: jjn
 * @Date: 2019/12/31 17:47
 * @Desc: 业务常量类
 */
public class BizConstant {

    //是否认证(0:是；1:否)
    public static final String IS_AUTH_YES = "0";
    public static final String IS_AUTH_NO = "1";

}
