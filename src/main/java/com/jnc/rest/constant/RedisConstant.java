package com.jnc.rest.constant;

/**
 * @Auther: jjn
 * @Date: 2020/4/9 14:51
 * @Desc:
 */
public class RedisConstant {

    // key: username  value:token redis前缀
    public static final String PRE_USER_TOKEN = "ut:";
    // key: token  value:username redis前缀
    public static final String PRE_TOKEN_USER = "tu:";
    // key: token  value:username redis前缀
    public static final String PRE_OFF_TOKEN_USER = "off_tu:";

    //字典值 hash key前缀；   key: dict:data:[dictType]    hashKey: dictCode    hashValue: dictValue
    public static final String DICT_DATA = "dict:data:";
    // uri认证 list key；    key：uri:auth:data     list: uri
    public static final String URI_AUTH_DATA = "uri:auth:data";

}
