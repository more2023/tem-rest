package com.jnc.rest.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.TypeReference;
import com.jnc.rest.constant.SysConstant;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @Auther: jjn
 * @Date: 2019/12/9 12:04
 * @Desc: Json工具类
 */
@Slf4j
@SuppressWarnings({ "rawtypes", "unchecked" })
public class JsonUtil {

    /**
     * json字符串转换成对象
     * @param jsonString
     * @param clazz
     * @return
     */
    public static <T> T json2Bean(String jsonString, Class<T> clazz){
        T t = null;
        try {
            t = JSON.parseObject(jsonString, clazz);
        } catch (Exception e) {
            log.error("JsonUtil.json2Bean Exception: {}", e);
        }
        return t;
    }

    /**
     * json字符串转换成泛型对象
     * @param jsonString
     * @param type
     * @param <T>
     * @return
     */
    public static <T> T json2Bean(String jsonString, TypeReference<T> type){
        T t = null;
        try {
            t = JSON.parseObject(jsonString, type);
        } catch (Exception e) {
            log.error("JsonUtil.json2Bean Exception: {}", e);
        }
        return t;
    }

    /**
     * 对象转换成json字符串
     * @param obj
     * @return
     */
    public static String bean2Json(Object obj){
        return JSON.toJSONString(obj);
    }

    /**
     * 对象转json Byte数组
     * @param obj
     * @return
     */
    public static byte[] bean2JsonBytes(Object obj) {
        //把对象转换成JSON
        String json = bean2Json(obj);
        try {
            return json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("JsonUtil.bean2JsonBytes Exception: {}", e);
        }
        return null;
    }

    /**
     * json字节数组转对象
     * @param bytes
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T jsonBytes2Bean(byte[] bytes, Class<T> clazz){
        String json = ConvertUtil.str(bytes, SysConstant.CHARSET_NAME);
        return json2Bean(json, clazz);
    }

    /**
     * json字符串转换成List集合
     * @param jsonString
     * @return
     */
   
	public static <T> List<T> json2List(String jsonString, Class clazz){
        List<T> list = null;
        try {
            list = JSON.parseArray(jsonString, clazz);
        } catch (Exception e) {
            log.error("JsonUtil.json2List Exception: {}", e);
        }
        return list;
    }

    /**
     * List集合转换成json字符串
     * @param obj
     * @return
     */
    public static String list2Json(Object obj){
        return JSONArray.toJSONString(obj, true);
    }

    /**
     * json转List
     * (不需要实体类)
     * @param jsonStr
     * @return
     */
    public static JSONArray json2List(String jsonStr){
        return JSON.parseArray(jsonStr);
    }
    
    /*
    public static void main(String[] art) {
        String dataJson="[{\"ringId\":\"91789000202005131170580\",\"ringName\":\"疫情期的升旗\",\"ringType\":\"1\",\"type\":\"1\",\"settingFlag\":\"0\",\"orderTime\":\"20200513162153\",\"ringIndex\":846351},{\"ringId\":\"91789000202005131170630\",\"ringName\":\"我是落汤鸡\",\"ringType\":\"1\",\"type\":\"1\",\"settingFlag\":\"1\",\"orderTime\":\"20200513162037\",\"ringIndex\":846346},{\"ringId\":\"79757000202004119096910\",\"ringName\":\"冰洁花骨朵儿\",\"ringType\":\"1\",\"type\":\"1\",\"settingFlag\":\"0\",\"orderTime\":\"20200504231717\",\"ringIndex\":740146},{\"ringId\":\"91789000202003260730480\",\"ringName\":\"儿子的小实验\",\"ringType\":\"1\",\"type\":\"1\",\"settingFlag\":\"0\",\"orderTime\":\"20200326150811\",\"ringIndex\":726279},{\"ringId\":\"90755000202003120715900\",\"ringName\":\"黑客入侵\",\"ringType\":\"1\",\"type\":\"1\",\"settingFlag\":\"0\",\"orderTime\":\"20200326113211\",\"ringIndex\":716539}]";
        List<RingCUInfo> ringInfos = JsonUtil.json2List(dataJson, RingCUInfo.class);
        System.err.println("test:infos:" + ringInfos.size());
        System.err.println("test:infos:" + JSON.toJSONString(ringInfos));
        int pos = 0;
        for(int i=0; i<ringInfos.size(); i++) {
            RingCUInfo info = ringInfos.get(i);
            if("1".equals(info.getSettingFlag())) {                               
                break;
            } 
            pos ++;
        }
        if(pos > 0 && pos < ringInfos.size()) {
            Collections.swap(ringInfos, pos, 0);
        }
        System.err.println("test:infos:" +JSON.toJSONString(ringInfos));
    }
    */
}
