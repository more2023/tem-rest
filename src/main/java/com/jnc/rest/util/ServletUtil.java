package com.jnc.rest.util;

import com.alibaba.fastjson.JSON;
import com.jnc.rest.constant.SysConstant;
import com.jnc.rest.core.base.dto.BaseResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Auther: jjn
 * @Date: 2018/8/11
 * @Desc: servlet工具类
 * Spring MVC中提供了一个非常便捷的工具类RequestContextHolder，
 * 能够在程序中，方便获取request对象和response对象
 */
@SuppressWarnings("rawtypes")
public class ServletUtil {
    private static final Logger logger = LoggerFactory.getLogger(ServletUtil.class);

    /**
     * 获取RequestAttributes对象
     * @return
     */
    public static ServletRequestAttributes getRequestAttributes(){
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    /**
     * 获取request
     * @return
     */
    public static HttpServletRequest getRequest(){
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取response
     * @return
     */
    public static HttpServletResponse getResponse(){
        return getRequestAttributes().getResponse();
    }

    /**
     * 获取session
     * @return
     */
    public static HttpSession getSession(){
        return getRequest().getSession();
    }

    /**
     * 获取String参数
     * @param name
     * @return
     */
    public static String getParameter(String name){
        return getRequest().getParameter(name);
    }

    /**
     * 获取String参数
     * @param name
     * @param defaultValue 默认值
     * @return
     */
    public static String getParameter(String name, String defaultValue){
        return ConvertUtil.toStr(getRequest().getParameter(name), defaultValue);
    }

    /**
     * 获取Integer参数
     * @param name
     * @return
     */
    public static Integer getParameterToInt(String name){
        return ConvertUtil.toInt(getRequest().getParameter(name));
    }

    /**
     * 获取Integer参数
     * @param name
     * @param defaultValue
     * @return
     */
    public static Integer getParameterToInt(String name, Integer defaultValue){
        return ConvertUtil.toInt(getRequest().getParameter(name), defaultValue);
    }

    /**
     * 响应数据写出到客户端
     * @param response
     * @param result
     */
    
	public static String responseResult(HttpServletResponse response, BaseResp result){
        response.setCharacterEncoding(SysConstant.CHARSET_NAME);
        response.setHeader("Content-type", "application/json;charset=UTF-8");
        response.setStatus(SysConstant.HTTP_CODE_SUCCESS);
        try {
            response.getWriter().write(JSON.toJSONString(result));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

}
