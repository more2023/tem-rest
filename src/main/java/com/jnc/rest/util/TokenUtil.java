package com.jnc.rest.util;

import com.jnc.rest.constant.RedisConstant;
import com.jnc.rest.core.config.redis.RedisStore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @Auther: jjn
 * @Date: 2020/1/6 17:21
 * @Desc: token工具类
 */
@Component
public class TokenUtil {

    public static final String salt = "WnkQ*^$1293Ie_As";

    @Autowired
    RedisStore redisStore;


    /**
     * 判断用户是否已登录
     * @param userName
     * @return
     */
    public boolean isLogin(String userName){
        //生成userName和token关联redis key
        String key = genUserTokenKey(userName);
        return redisStore.exists(key);
    }

    /**
     * 根据用户名，获取token缓存
     * @param userName
     * @return
     */
    public String getTokenBy(String userName){
        //生成userName和token关联redis key
        String key = genUserTokenKey(userName);
        return (String) redisStore.get(key);
    }

    /**
     * 根据token，获取用户名缓存
     * @param token
     * @return
     */
    public String getUserNameBy(String token){
        String key = genTokenUserKey(token);
        return (String) redisStore.get(key);
    }

    /**
     * 获取 key: token  value: userName的有效时长
     * @return
     */
    public long getDuration(String token){
        String key = genTokenUserKey(token);
        return redisStore.getExpire(key);
    }

    /**
     * 根据用户名，生成token
     * @param userName
     * @return
     */
    public String genToken(String userName){
        String temp = ConvertUtil.toStr(System.currentTimeMillis());
        String substring = temp.substring(temp.length() - 4, temp.length());
        try {
            return AESUtil.aesEncrypt(userName + ":" + substring, salt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据token，解析获取用户名
     * @param token
     * @return
     */
    public String getUserName(String token){
        try {
            String decode = AESUtil.aesDecrypt(token, salt);
            if(StringUtils.isNotBlank(decode)){
                String[] split = decode.split(":");
                return split[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 生成userName和token关联redis key， key: userName，value: token
     * @param userName
     * @return
     */
    public String genUserTokenKey(String userName){
        return RedisConstant.PRE_USER_TOKEN + userName;
    }

    /**
     * 生成token和userName关联redis key， key: token，value: userName
     * @param token
     * @return
     */
    public String genTokenUserKey(String token){
        return RedisConstant.PRE_TOKEN_USER + token;
    }

    /**
     * 生成踢人redis key， key: token，value: userName
     * @param token
     * @return
     */
    public String genOffTokenUserKey(String token){
        return RedisConstant.PRE_OFF_TOKEN_USER + token;
    }


    public static void main(String[] args){
        TokenUtil to = new TokenUtil();
        String admin = to.genToken("admin");
        System.out.println(admin);
    }
}
