package com.jnc.rest;

import com.jnc.rest.core.des.annotation.EnableEncrypt;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @Auther: jjn
 * @Date: 2019/12/23 18:16
 * @Desc:
 */
@Slf4j
@EnableEncrypt
@EnableAsync
@SpringBootApplication
@MapperScan("com.jnc.rest.**.mapper")
public class WebServer {

    public static void main(String[] args) {
        SpringApplication.run(WebServer.class, args);
        log.info("Server starting ...");
    }
}
