package com.jnc.rest.core.des.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jnc.rest.core.des.annotation.Encrypt;
import com.jnc.rest.core.des.auto.EncryptProperties;
import com.jnc.rest.util.AESUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @Auther: jjn
 * @Date: 2020/2/13 14:41
 * @Desc: 响应参数加密处理类
 */
@Slf4j
@ControllerAdvice
public class EncryptResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    @Autowired
    private EncryptProperties encryptProperties;

    private ObjectMapper objectMapper = new ObjectMapper();

    private static ThreadLocal<Boolean> encryptLocal = new ThreadLocal<Boolean>();

    public static void setEncryptStatus(boolean status) {
        encryptLocal.set(status);
    }

    @Override
    public boolean supports(MethodParameter methodParameter,
                            Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest request, ServerHttpResponse response) {

        // 可以通过调用EncryptResponseBodyAdvice.setEncryptStatus(false);来动态设置不加密操作
        Boolean status = encryptLocal.get();
        if (status != null && status == false) {
            encryptLocal.remove();
            return body;
        }

        long startTime = System.currentTimeMillis();

        if(methodParameter.getMethod().isAnnotationPresent(Encrypt.class) && encryptProperties.isOpen()){
            try {
                String content = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
                if (StringUtils.isBlank(encryptProperties.getSalt())) {
                    throw new NullPointerException("请配置param.encrypt.salt");
                }
                String result = AESUtil.aesEncrypt(content, encryptProperties.getSalt());
                long endTime = System.currentTimeMillis();
                log.debug("Encrypt Time: {}, content: {}, result: {}", (endTime - startTime), content, result);
                return result;
            } catch (Exception e) {
                log.error("param encrypt error, method: {}, exception: {}",
                        methodParameter.getMethod().getName(), e);
            }
        }
        return body;
    }
}
