package com.jnc.rest.core.des.auto;

import com.jnc.rest.constant.SysConstant;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @Auther: jjn
 * @Date: 2020/2/13 11:21
 * @Desc: 请求响应参数加解密资源类
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "param.encrypt")
public class EncryptProperties {
    //加密盐值
    private String salt;

    private String charset = SysConstant.CHARSET_NAME;
    //是否开启加解密
    private boolean open = false;

}
