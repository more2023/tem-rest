package com.jnc.rest.core.des.annotation;

import com.jnc.rest.core.des.auto.EncryptAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Auther: jjn
 * @Date: 2020/2/13
 * @Desc: 开启加解密注解
 * 在Spring Boot启动类上加上此注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({EncryptAutoConfiguration.class})
public @interface EnableEncrypt {
}
