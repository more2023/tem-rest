package com.jnc.rest.core.des.auto;

import com.jnc.rest.core.des.advice.DecryptRequestBodyAdvice;
import com.jnc.rest.core.des.advice.EncryptResponseBodyAdvice;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


/**
 * @Auther: jjn
 * @Date: 2020/2/13
 * @Desc: 加解密自动配置
 */
@Configuration
@Component
@EnableAutoConfiguration
@EnableConfigurationProperties(EncryptProperties.class)
public class EncryptAutoConfiguration {

    /**
     * 请求解密
     * @return
     */
    @Bean
    public DecryptRequestBodyAdvice decryptRequestBodyAdvice(){
        return new DecryptRequestBodyAdvice();
    }

    /**
     * 响应加密
     * @return
     */
    @Bean
    public EncryptResponseBodyAdvice encryptResponseBodyAdvice(){
        return new EncryptResponseBodyAdvice();
    }
}
