package com.jnc.rest.core.des.annotation;

import java.lang.annotation.*;

/**
 * @Auther: jjn
 * @Date: 2020/2/13
 * @Desc: 解密注解类
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Decrypt {
}
