package com.jnc.rest.core.error;

/**
 * @Auther: jjn
 * @Date: 2019/12/23 18:27
 * @Desc: 业务异常类
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;


    public ServiceException(){}


    public ServiceException(String message){
        super(message);
    }


    public ServiceException(String message, Throwable cause){
        super(message, cause);
    }
}
