package com.jnc.rest.core.error;

/**
 * @Auther: jjn
 * @Date: 2019/12/23 18:27
 * @Desc: 身份认证异常类
 */
public class AuthException extends RuntimeException {

    public AuthException(){}


    public AuthException(String message){
        super(message);
    }


    public AuthException(String message, Throwable cause){
        super(message, cause);
    }
}
