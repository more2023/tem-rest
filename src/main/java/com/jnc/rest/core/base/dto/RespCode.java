package com.jnc.rest.core.base.dto;

/**
 * @Auther: jjn
 * @Date: 2019/12/24
 * @Desc: 响应状态枚举类
 */
public enum RespCode {

    SUCCESS(0,"SUCCESS"),
    ERROR(1,"ERROR"),
    ILLEGAL_ARGUMENT(2,"非法参数"),
    KICK_LOGIN(10,"踢出登录");
    
    private final int code;
    private final String desc;

    RespCode(int code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }

    public String getDesc(){
        return desc;
    }
}
