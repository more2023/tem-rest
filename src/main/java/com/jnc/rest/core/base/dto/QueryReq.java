package com.jnc.rest.core.base.dto;

import com.jnc.rest.util.ConvertUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @Author: jjn
 * @Date: 2018/6/29
 * @Desc: 基础查询req
 */
public class QueryReq implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer pageNo;   //页码，从1开始
    private Integer pageSize;  //每页数量，取值范围1-200
    private String orderByColumn;   //排序列
    private String orderRule;       //排序规则(desc 或 asc)

    public Integer getPageNo() {
        return pageNo;
    }

    public QueryReq setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public QueryReq setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public String getOrderByColumn() {
        return orderByColumn;
    }

    public QueryReq setOrderByColumn(String orderByColumn) {
        this.orderByColumn = orderByColumn;
        return this;
    }

    public String getOrderRule() {
        return orderRule;
    }

    public QueryReq setOrderRule(String orderRule) {
        this.orderRule = orderRule;
        return this;
    }

    /**
     * 获取排序拼接字段(java字段需要转换为数据库字段，驼峰命名转下划线)
     * @return
     */
    public String getOrderBy(){
        if(StringUtils.isEmpty(orderByColumn)){
            return "";
        }
        return ConvertUtil.camelToUnderline(orderByColumn) + " " + orderRule;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("pageNo", getPageNo())
                .append("pageSize", getPageSize())
                .append("orderByColumn", getOrderByColumn())
                .append("orderRule", getOrderRule())
                .toString();
    }
}
