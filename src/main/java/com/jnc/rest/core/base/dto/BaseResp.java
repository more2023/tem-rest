package com.jnc.rest.core.base.dto;

import com.jnc.rest.util.JsonUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @Author: jjn
 * @Date: 2018/7/6
 * @Desc: 基础resp
 */
@SuppressWarnings("rawtypes")
public class BaseResp<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;  //状态码  0：表示成功 1: 失败
    private String msg;   //提示信息
    private T data;       //返回数据

    public BaseResp() {
    }

    public BaseResp(int code) {
        this.code = code;
    }

    public BaseResp(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseResp(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public BaseResp(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> BaseResp<T> success(){
        return new BaseResp<T>(RespCode.SUCCESS.getCode(), RespCode.SUCCESS.getDesc());
    }

    public static <T> BaseResp<T> success(String msg){
        return new BaseResp<T>(RespCode.SUCCESS.getCode(), msg);
    }

    public static <T> BaseResp<T> success(T data){
        return new BaseResp<T>(RespCode.SUCCESS.getCode(), data);
    }

    public static <T> BaseResp<T> success(String msg, T data){
        return new BaseResp<T>(RespCode.SUCCESS.getCode(), msg, data);
    }

    public static <T> BaseResp<T> fail(){
        return new BaseResp<T>(RespCode.ERROR.getCode(), RespCode.ERROR.getDesc());
    }

    public static <T> BaseResp<T> fail(String msg){
        return new BaseResp<T>(RespCode.ERROR.getCode(), msg);
    }

    public static <T> BaseResp<T> fail(T data){
        return new BaseResp<T>(RespCode.ERROR.getCode(), data);
    }

    public static <T> BaseResp<T> fail(String msg, T data){
        return new BaseResp<T>(RespCode.ERROR.getCode(), msg, data);
    }

    public static <T> BaseResp<T> resp(int code, String msg, T data){
        return new BaseResp<T>(code, msg, data);
    }

    public int getCode() {
        return code;
    }

    
    public BaseResp setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public BaseResp setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public T getData() {
        return data;
    }

    public BaseResp setData(T data) {
        this.data = data;
        return this;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("code", getCode())
                .append("msg", getMsg())
                .append("data", JsonUtil.bean2Json(getData()))
                .toString();
    }
}
