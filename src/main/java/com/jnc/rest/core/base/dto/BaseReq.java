package com.jnc.rest.core.base.dto;

import java.io.Serializable;

/**
 * @Author: jjn
 * @Date: 2018/11/14
 * @Desc: 基础 req
 */
public class BaseReq implements Serializable {
    private static final long serialVersionUID = 1L;
}
