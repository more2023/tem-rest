package com.jnc.rest.core.load;

import com.jnc.rest.biz.common.service.DictService;
import com.jnc.rest.core.config.redis.RedisStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * @Author: jjn
 * @Date: 2019/2/27 17:49
 * @Desc: 加载字典值到redis
 */
@Slf4j
@Component
@Order(value = 1)
public class LoadDictData implements CommandLineRunner {

    @Autowired
    DictService dictService;
    @Autowired
    RedisStore redisStore;

    @Override
    public void run(String... args) throws Exception {
        log.debug("Start load dict data to redis ... ");
        long start = System.currentTimeMillis();

        //清除旧数据缓存，保存新数据缓存
        dictService.loadRedisDictData();

        long end = System.currentTimeMillis();
        log.debug("End load dict data to redis, cost time: {}", end - start);
    }
}
