package com.jnc.rest.core.load;

import com.jnc.rest.biz.common.mapper.DictMapper;
import com.jnc.rest.biz.common.model.UriAuth;
import com.jnc.rest.constant.RedisConstant;
import com.jnc.rest.core.config.redis.RedisStore;
import com.jnc.rest.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;


/**
 * @Author: jjn
 * @Date: 2019/2/27 17:49
 * @Desc: 加载认证URI到redis
 */
@Slf4j
@Component
@Order(value = 1)
public class LoadUriAuthData implements CommandLineRunner {

    @Autowired
    DictMapper dictMapper;
    @Autowired
    RedisStore redisStore;

    @Override
    public void run(String... args) throws Exception {
        log.debug("Start load uri auth data to redis ... ");
        long start = System.currentTimeMillis();

        //清除旧数据缓存，保存新数据缓存
        List<UriAuth> uriList = dictMapper.queryUri();

        redisStore.del(RedisConstant.URI_AUTH_DATA);
        //保存到redis中
        if(uriList != null && uriList.size() > 0){
            Iterator<UriAuth> it = uriList.iterator();
            while (it.hasNext()){
                redisStore.leftPush(RedisConstant.URI_AUTH_DATA, JsonUtil.bean2Json(it.next()));
            }
        }

        long end = System.currentTimeMillis();
        log.debug("End load uri auth data to redis, cost time: {}", end - start);
    }
}
